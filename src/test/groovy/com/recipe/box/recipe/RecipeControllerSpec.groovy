package com.recipe.box.recipe


import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class RecipeControllerSpec extends Specification {


    /***
     * @SpringBean annotation is Spock's alternative to @MockBean
     * Used Stub instead of Mock, because counting method calls is unnecessary
    */
    @SpringBean
    private RecipeRepository recipes = Stub()

    @Autowired
    private RecipeController controller

    def "getRecipe test"() {
        given:
            Recipe recipe = new Recipe(
                    id: 1L,
                    title: "A Title"
            )
            recipes.findById(1L) >> Optional.of(recipe)

        expect:
            controller.getRecipe(1L) == recipe
    }
}
