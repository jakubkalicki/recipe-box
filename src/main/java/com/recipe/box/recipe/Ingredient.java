package com.recipe.box.recipe;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class Ingredient {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private int amount;
    @Enumerated(EnumType.STRING)
    private UnitType unitType;

    /**
     * Suggested product from open-source database database
     * https://pl.openfoodfacts.org/
     */
    private Long productBarcode;
    @ManyToMany(mappedBy = "ingredients")
    private Set<Recipe> recipes;

}
