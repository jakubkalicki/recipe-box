package com.recipe.box.recipe;

import com.recipe.box.common.exception.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class RecipeController {

    private RecipeRepository recipes;

    public RecipeController(RecipeRepository recipes) {
        this.recipes = recipes;
    }

    @GetMapping("/recipes/{recipeId}")
    public Recipe getRecipe(@PathVariable Long recipeId) {
        return recipes.findById(recipeId)
                .orElseThrow(() -> new ResourceNotFoundException("Recipe not found with id " + recipeId));
    }

    @GetMapping("/recipes")
    public Page<Recipe> getRecipes(Pageable pageable) {
        return recipes.findAll(pageable);
    }

    @PostMapping("/recipes")
    public Recipe createRecipe(@Valid @RequestBody Recipe recipe) {
        return recipes.save(recipe);
    }

    @PutMapping("/recipes/{recipeId}")
    public Recipe updateRecipe(@PathVariable Long recipeId,
                               @Valid @RequestBody Recipe recipeRequest) {
        return recipes.findById(recipeId)
                .map(recipe -> {
                    recipe.setTitle(recipeRequest.getTitle());
                    recipe.setIngredients(recipeRequest.getIngredients());
                    recipe.setActions(recipeRequest.getActions());
                    return recipes.save(recipe);
                }).orElseThrow(() -> new ResourceNotFoundException("Recipe not found with id " + recipeId));
    }

    @DeleteMapping("recipes/{recipeId}")
    public ResponseEntity<?> deleteRecipe(@PathVariable Long recipeId) {
        return recipes.findById(recipeId)
                .map(recipe -> {
                    recipes.delete(recipe);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Recipe not found with id " + recipeId));
    }

}
