package com.recipe.box.recipe;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@Entity
public class Recipe {

    @Id
    @GeneratedValue
    private Long id;
    private String title;
    @ManyToMany
    @JoinTable(
            name = "Recipes_ingridients",
            joinColumns = @JoinColumn(name = "recipe_id"),
            inverseJoinColumns = @JoinColumn(name = "ingredient_id")
    )
    private Set<Ingredient> ingredients;

    @OneToMany(mappedBy = "recipe")
    private List<Action> actions;
}
