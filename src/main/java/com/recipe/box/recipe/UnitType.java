package com.recipe.box.recipe;

enum UnitType {
    G,
    MG,
    KG,
    ML,
    L
}
