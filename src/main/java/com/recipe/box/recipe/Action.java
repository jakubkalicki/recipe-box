package com.recipe.box.recipe;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Action {

    @Id
    @GeneratedValue
    private Long id;
    private String text;

    @ManyToOne
    @JoinColumn(name = "recipe_id")
    private Recipe recipe;

}
