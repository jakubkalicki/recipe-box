# Pull maven 3.6 with jdk 8 based on Alpine (minimal Linux distro ~5MB)
FROM maven:3.6-jdk-8-alpine AS builder

# Set or create working directory
WORKDIR /recipe-box
COPY pom.xml .

# Download dependencies if missing
# -e to show errors
# -B to hide color characters
RUN mvn -e -B dependency:resolve
COPY src ./src
RUN mvn -e -B package


# Copy application jar from intermediate container named "builder"
FROM openjdk:8-jre-alpine
COPY --from=builder /recipe-box/target/recipe-box-0.0.1-SNAPSHOT.jar recipe-box.jar

# Open container's port for the application
EXPOSE 8080

# Start application
CMD java -jar recipe-box.jar