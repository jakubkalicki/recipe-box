# Recipe Box

Recipe storage application

Pet project for the purpose of gaining knowledge about specific tools, frameworks etc

Current tech stack:
- Java 8
- Spring Boot
- Hibernate
- Spock
- Docker
- Gitlab CI/CD
- Maven

To be added (proposals):
- Vue.js / React.js / Angular.js
- PostgresSQL
- Swagger + OpenAPI
- ElasticSearch
