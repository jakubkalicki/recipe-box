#!/usr/bin/bash
declare image_name="${1}";
declare container_name="${2}";

docker build . --tag ${image_name};

echo Delete old container...
docker rm -f ${container_name};

echo Run new container...
docker run -t -p 8080:8080 --name ${container_name} ${image_name};